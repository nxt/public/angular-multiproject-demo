import {Component} from '@angular/core';

export interface Project {
  name: string;
  budget: number;
  startDate: Date;
  technology: string;
}

const PROJECT_DATA: Project[] = [
  {budget: 88000, name: 'Apollo', startDate: new Date(2017, 2, 1), technology: 'Angular'},
  {budget: 140000, name: 'Insurance 3.0', startDate: new Date(2018, 11, 1), technology: 'Java'},
  {budget: 112000, name: 'Digital Forward', startDate: new Date(2019, 9, 1), technology: 'Docker'},
];

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
})
export class AppComponent {
  displayedColumns: string[] = ['name', 'budget', 'startDate', 'technology'];
  dataSource = PROJECT_DATA;
}
